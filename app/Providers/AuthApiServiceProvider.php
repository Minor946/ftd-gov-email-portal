<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;

class AuthApiServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $domain = config('gov.domain') ?: env('MIX_API_DOMAIN');
        $username = config('gov.username') ?: env('API_USERNAME');
        $password = env('API_PASSWORD');

        if ($domain && $username && $password) {
            $client = new Client([
                'verify' => false,
            ]);

            try {
                $res = $client->post("{$domain}/api/suroo/auth", [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                    'form_params' => [
                        'username' => $username,
                        'password' =>  $password
                    ]
                ]);
            } catch (RequestException $e) {
                $res = $e->getResponse();
                // Log::error($e->getMessage());
            }

            if ($res && $res->getStatusCode() == 200) {
                $result = json_decode($res->getBody(), true);
                // Log::info($res->getBody());
                Cookie::queue('accessToken', $result['access_token'], $result['expires_in'], '/', request()->getHttpHost(), null, false);
                return $result;
            } else {
                // TODO: Remove or handle error properly after CI setup is over
                echo '<!--';
                print($e->getMessage());
                echo '-->';
            }
        }
    }
}
