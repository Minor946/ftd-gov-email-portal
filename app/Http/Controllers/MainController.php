<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

function parse_signed_request($signed_request) {
  list($encoded_sig, $payload) = explode('.', $signed_request, 2);

  $secret = "218f48ac424e454ce830895d67ad7ff9"; // Use your app secret here

  // decode the data
  $sig = base64_url_decode($encoded_sig);
  $data = json_decode(base64_url_decode($payload), true);

  // confirm the signature
  $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
  if ($sig !== $expected_sig) {
    error_log('Bad Signed JSON signature!');
    return null;
  }

  return $data;
}

function base64_url_decode($input) {
  return base64_decode(strtr($input, '-_', '+/'));
}

class MainController extends Controller
{

  public function home()
  {
    return view('welcome');
  }

  public function deletion() {
    header('Content-Type: application/json');

    if (array_key_exists('signed_request', $_POST)) {
      $signed_request = $_POST['signed_request'];
      $data = parse_signed_request($signed_request);
      if ($data) {
        $user_id = $data['user_id'];
        $client = new Client([
            'verify' => false,
        ]);

        $formData = [];
        $formData['snType'] = 'FB';

        $formParams = array_merge($formData, $data);

        try {
            $domain = config('gov.domain') ?: env('MIX_API_DOMAIN');

            $res = $client->post("{$domain}/api/suroo/citizens/deletion", [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => $formParams
            ]);
        } catch (RequestException $e) {
            $res = $e->getResponse();
            print($res);
        }
      }
    } else {
      $user_id = 'NOTFOUND';
    }

    // Start data deletion

    $status_url = 'https://kattar.tunduk.kg/deletion?id=' . $user_id; // URL to track the deletion
    $confirmation_code = $user_id; // unique code for the deletion request

    $data = array(
      'url' => $status_url,
      'confirmation_code' => $confirmation_code
    );
    echo json_encode($data);
  }
}
