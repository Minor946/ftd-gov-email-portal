import Vue from "vue";
import { setAuthToken } from '../utils/auth.service'

let actions = {
    auth({ commit }, { username, password, self }) {
        axios
            .post("api/suroo/auth", {
                username: username,
                password: password
            })
            .then(response => {
                if (response.data && response.data.access_token) {
                    setAuthToken(response.data);
                    self.$router.push({ name: "Dashboard" });
                } else {
                    if (response.data.message) {
                        Vue.$toast.open({
                            message: response.data.message,
                            type: "error"
                        });
                    }
                }
            })
            .catch(function(error) {
                Vue.$toast.open({
                    message: error.message,
                    type: "error"
                });
            });
    },
    createUser({ commit }, { data, self }) {
        axios
            .put("api/suroo/users/" + data.email, {
                description: data.description,
                lang: data.lang,
                roles: data.roles
            })
            .then(response => {
                if (response.data && response.data.token)
                    self.$router.push({ name: "Users" });
            });
    },
    updateUser({ commit }, { data, self }) {
        axios
            .post("api/suroo/users/" + data.email, {
                description: data.description,
                lang: data.lang,
                enabled: data.enabled,
                roles: data.roles
            })
            .then(response => {
                if (response.data && response.data.token)
                    self.$router.push({ name: "Users" });
            });
    }
};

export default actions;
