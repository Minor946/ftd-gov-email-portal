export const ROLE_ADMIN = 'ROLE_ADMIN';
export const ROLE_USER = 'ROLE_USER';

export const roles = [
    ROLE_ADMIN,
    ROLE_USER
]

export const tokenName = 'adminToken';
