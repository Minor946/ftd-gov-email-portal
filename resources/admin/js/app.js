require("./bootstrap");

window.Vue = require("vue");

import App from "./App.vue";
import router from "./router";
import store from "./store/index";
import VueCookies from "vue-cookies";
import VueAxios from "vue-axios";
import axios from "axios";
import CoreuiVue from "@coreui/vue";
import CoreuiVueCharts from "@coreui/vue-chartjs";
import { iconsSet as icons } from "./assets/icons/icons.js";
import Vuelidate from 'vuelidate'
import VueToast from 'vue-toast-notification';
import {tokenName} from './const';

Vue.use(VueCookies);
Vue.use(Vuelidate);
Vue.use(VueToast);

window.axios.defaults.headers.common["Authorization"] =
    "Bearer " + Vue.$cookies.get(tokenName);
window.axios.defaults.headers.common["Content-Type"] = "application/json";

Vue.use(VueAxios, axios);
Vue.use(CoreuiVue);
Vue.use(CoreuiVueCharts);

new Vue({
    router,
    render: h => h(App),
    store,
    icons
}).$mount(`#home`);
