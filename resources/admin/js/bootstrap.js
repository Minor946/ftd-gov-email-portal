window._ = require("lodash");
moment = require("moment");
momentDurationFormatSetup = require("moment-duration-format");

moment.locale("ru");

window.axios = require("axios");
window.convertText = require("convert-layout");

window.axios.defaults.baseURL = process.env.MIX_API_DOMAIN;
window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
window.axios.defaults.headers.common["Content-Type"] = "application/json";
window.axios.defaults.headers.common["Accept"] = "application/json";
window.axios.defaults.timeout = 5000;
