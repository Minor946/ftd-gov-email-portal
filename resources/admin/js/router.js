import Vue from "vue";
import Router from "vue-router";
import TheContainer from "./containers/TheContainer";
import Dashboard from "./views/Dashboard";
import Login from "./views/pages/Login";
import Goverments from "./views/pages/Goverments";
import Users from "./views/pages/Users";
import { isLoggedIn } from "./utils/auth.service";

Vue.use(Router);


var router = new Router({
    routes: configRoutes(),
    mode: "history"
});

router.beforeEach((to, from, next) => {
    if (!to.meta.allowAnonymous && !isLoggedIn()) {
        next({
            path: "/administrator/login",
            query: { redirect: to.fullPath }
        });
    } else {
        next();
    }
});

export default router;

function configRoutes() {
    return [
        {
            path: "/administrator/",
            redirect: "/administrator/login",
            name: "Home",
            component: TheContainer,
            children: [
                {
                    path: "/administrator/dashboard",
                    name: "Dashboard",
                    component: Dashboard,
                    meta: {
                        allowAnonymous: false
                    }
                },
                {
                    path: "/administrator/users",
                    name: "Users",
                    component: Users,
                    meta: {
                        allowAnonymous: false
                    }
                },
                {
                    path: "/administrator/goverments",
                    name: "Goverments",
                    component: Goverments,
                    meta: {
                        allowAnonymous: false
                    }
                }
            ]
        },
        {
            path: "/administrator/login",
            name: "Login",
            component: Login,
            meta: {
                allowAnonymous: true
            }
        }
    ];
}
