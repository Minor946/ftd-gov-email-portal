export const ERROR_CODE_MESSAGE = [
  {
    id: 0,
    name: 'userId not found'
  },
  {
    id: 1,
    name: 'BIO is empty'
  },
  {
    id: 2,
    name: 'Contact is empty'
  },
  {
    id: 3,
    name: "Contact isn't valid email or phone number"
  },
  {
    id: 4,
    name: 'departmentId is empty'
  },
  {
    id: 5,
    name: 'Theme is empty'
  },
  {
    id: 6,
    name: 'message is empty'
  },
  {
    id: 7,
    name: 'file too large'
  },
  {
    id: 8,
    name: 'user code is incorrect'
  },
  {
    id: 9,
    name: 'social net type is empty'
  },
  {
    id: 10,
    name: 'user token is empty'
  },
  {
    id: 11,
    name: 'user contact not found'
  }
];

export const tokenName = 'accessToken';

export const backendUrl = process.env.MIX_API_DOMAIN;
