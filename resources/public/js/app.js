require("./bootstrap");

window.Vue = require("vue");

import App from "./App.vue";
import router from "./router";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import store from "./store/index";
import VueCookies from "vue-cookies";
import VueAxios from "vue-axios";
import VueSocialauth from "vue-social-auth";
import vSelect from "vue-select";
import AuthModal from "./components/modals/AuthModal.vue";
import HowItWorksModal from "./components/modals/HowItWorksModal.vue";
import ValidateModal from "./components/modals/ValidateModal";
import axios from "axios";
import Notifications from "vue-notification";
import Vuelidate from "vuelidate";
import {tokenName} from "./const";
import i18n from './i18n';
import {
    LayoutPlugin,
    ModalPlugin,
    CardPlugin,
    TablePlugin,
    ButtonPlugin,
    ButtonGroupPlugin,
    CollapsePlugin,
    NavbarPlugin,
    FormPlugin,
    InputGroupPlugin,
    NavPlugin,
    JumbotronPlugin,
    FormTextareaPlugin,
    FormInputPlugin,
    FormFilePlugin,
    FormGroupPlugin,
    PaginationPlugin,
    SpinnerPlugin,
} from "bootstrap-vue";
import VueSmoothScroll from 'vue2-smooth-scroll'

Vue.use(VueSmoothScroll, {
  duration: 400,
  updateHistory: false,
  offset: -100,
})

Vue.use(LayoutPlugin);
Vue.use(ModalPlugin);
Vue.use(CardPlugin);
Vue.use(TablePlugin);
Vue.use(ButtonPlugin);
Vue.use(ButtonGroupPlugin);
Vue.use(NavbarPlugin);
Vue.use(CollapsePlugin);
Vue.use(FormPlugin);
Vue.use(InputGroupPlugin);
Vue.use(NavPlugin);
Vue.use(JumbotronPlugin);
Vue.use(FormTextareaPlugin);
Vue.use(FormInputPlugin);
Vue.use(FormFilePlugin);
Vue.use(FormGroupPlugin);
Vue.use(PaginationPlugin);
Vue.use(SpinnerPlugin);

library.add(faPaperPlane);
Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.use(Notifications)
Vue.use(VueCookies)
Vue.use(Vuelidate)

window.axios.defaults.headers.common["Authorization"] =
    "Bearer " + Vue.$cookies.get(tokenName);

Vue.use(VueAxios, axios);

Vue.use(VueSocialauth, {
    providers: {
        facebook: {
            clientId: process.env.MIX_FACEBOOK_CLIENT_ID,
            redirectUri: "https://kattar.tunduk.kg/"
        },
        instagram: {
            clientId: process.env.MIX_INSTAGRAM_CLIENT_ID,
            redirectUri: "/"
        },
        twitter: {
            clientId: process.env.MIX_TWITTER_CLIENT_ID,
            redirectUri: "/"
        },
        vkontakte: {
            clientId: process.env.MIX_VKONTAKTE_CLIENT_ID,
            redirectUri: "/"
        }
    }
});
Vue.component("v-select", vSelect);
Vue.component("modal-validate-component", ValidateModal);
Vue.component("modal-auth-component", AuthModal);
Vue.component("modal-howitworks-component", HowItWorksModal);

Vue.mixin({
    methods: {
        goTo: function (path) {
            if (this.$route.path !== path) {
                this.$router.push(path || '/')
            }
            const el = document.getElementById('logo');
            if(el) {
                this.$smoothScroll({
                    scrollTo: el,
                })
            }
        },
        goToForm: function () {
            if (this.$route.path !== '/') {
                this.$router.push('/')
            }
            this.$nextTick(() => {
                const el = document.getElementById('section-send');
                if(el) {
                    this.$smoothScroll({
                        scrollTo: el,
                    })
                }
            })
        },
        authUser: function () {
            if (this.$store.state.userUuid && this.$store.state.userSession) {
                if (this.$route.path !== '/list') {
                    this.$router.push({ name: "list" });
                }
                const el = document.getElementById('logo');
                if(el) {
                    this.$smoothScroll({
                        scrollTo: el,
                    })
                }
            } else {
                this.$bvModal.show("modal-auth");
            }
        }
    },
})

new Vue({
    router,
    render: h => h(App),
    i18n,
    store
}).$mount(`#home`);
