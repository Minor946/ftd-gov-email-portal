import i18n from '../i18n.js'

let state = {
    token: null,
    userUuid: null,
    userSession: null,
    userContact: null,
    goverments: [
        {
            name:
                "Государственная регистрационная служба при Правительстве Кыргызской Республики",
            id: 1
        }
    ],
    stats: [],
    themeTypes: [
        {
            id: 0,
            name: i18n.t('types.request')
        },
        {
            id: 1,
            name: i18n.t('types.offer')
        },
        {
            id: 2,
            name: i18n.t('types.appeal')
        }
    ],
    openedMessage: null,
    showValidateModal: false,
    showSuccessModal: false,
    showSuccess: true,
    themeList: []
};

export default state;
