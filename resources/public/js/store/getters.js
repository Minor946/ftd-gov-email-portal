let getters = {
  authorized: (state) => state.userUuid && state.userSession
};

export default getters;
