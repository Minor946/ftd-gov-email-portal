let mutations = {
    SET_USER_UUID(state, uuid, showSuccess) {
        state.userUuid = uuid;
        state.showSuccess = showSuccess;
        return state;
    },
    SET_USER_SESSION(state, uuid) {
        return (state.userSession = uuid);
    },
    SET_USER_CONTACT(state, contact) {
        return (state.userContact = contact);
    },
    RESET_USER(state) {
        localStorage.removeItem('userUuid');
        localStorage.removeItem('userSession');
        localStorage.removeItem('userContact');

        state.userUuid = null;
        state.userSession = null;
        state.userContact = null;
        state.token = null;
    },
    UPDATE_GOVERMENT_LIST(state, data) {
        return (state.goverments = data);
    },
    UPDATE_STATS(state, data) {
        return (state.stats = data);
    },
    SET_USER_THEMES_LIST(state, data) {
        return (state.themeList = data);
    },
    SHOW_ADDED_SOCIAL(state, modal) {
        Vue.$bvModal.msgBoxOk("Social added", {
            title: "Confirmation",
            size: "sm",
            buttonSize: "sm",
            okVariant: "success",
            headerClass: "p-2 border-bottom-0",
            footerClass: "p-2 border-top-0",
            centered: true
        });
    },
    SHOW_NEW_CODE(state) { },
    OPEN_MESSAGE(state, msg) {
        state.openedMessage = msg;
    },
    CLOSE_MESSAGE(state) {
        state.openedMessage = null;
    }

};
export default mutations;
