import Vue from "vue";
import { backendUrl } from "../const";

let actions = {
    init({ commit }) {
        const uuid = localStorage.getItem('userUuid');
        const session = localStorage.getItem('userSession');
        const contact = localStorage.getItem('userContact');
        if (uuid && session && contact) {
            commit('SET_USER_UUID', uuid, false);
            commit('SET_USER_SESSION', session);
            commit('SET_USER_CONTACT', contact);
        }
    },
    logout({ commit }) {
        commit('RESET_USER');
    },
    getStats({ commit, state }) {
        if (state.stats.length > 1) return; //force-prevent duplicate calls for stats
        axios
            .get(`${backendUrl}/api/suroo/stats/all`)
            .then(response => {
                commit("UPDATE_STATS", response.data);
            })
            .catch(function(error) {
                console.log(error);
            });
    },
    getGoverments({ commit, state }) {
        if (state.goverments.length > 1) return; //force-prevent duplicate calls for gov list
        axios
            .get(`${backendUrl}/api/suroo/department/list`)
            .then(response => {
                commit("UPDATE_GOVERMENT_LIST", response.data);
            })
            .catch(function(error) {
                console.log(error);
            });
    },
    sendMessage({ commit }, { data, self }) {
        var imagefile = document.querySelector("#input-file");
        axios
            .post(
                `${backendUrl}/api/suroo/citizens/messageJson`,
                {
                    userBio: data.userBio,
                    userContact: data.userContact,
                    departmentId: data.departmentId,
                    theme: data.theme,
                    message: data.message,
                    sedType: data.typeId
                },
                {
                    headers: {
                        "Content-Type": "application/json"
                    }
                }
            )
            .then(response => {
                console.log(response);
                if (response.data.userUuid) {
                    commit("SET_USER_UUID", response.data.userUuid, true);
                    self.$bvModal.show("modal-validate");
                }
            })
            .catch(function(error) {
                if (error.response) {
                    if (error.response.code === 400) {
                        self.$notify({
                            group: "notify",
                            type: "error",
                            title: "Error",
                            text: error.message,
                            duration: 10000,
                            speed: 1000
                        });
                    } else {
                        self.$notify({
                            group: "notify",
                            type: "error",
                            title: "Error",
                            text: `Сервер вернул ответ с кодом ${error.response.code}`,
                            duration: 10000,
                            speed: 1000
                        });

                    }
                }
            });
    },
    sendVerifierCode({ commit, state }, { userCode, self }) {
        axios
            .post(`${backendUrl}/api/suroo/citizens/verifier`, {
                userUuid: state.userUuid,
                userCode
            })
            .then(response => {
                if (response.data.userSession) {
                    localStorage.setItem('userSession', response.data.userSession);
                    commit("SET_USER_SESSION", response.data.userSession);
                    self.$bvModal.hide("modal-validate");
                    if (state.showSuccess) {
                        self.$bvModal.msgBoxOk("Messege send", {
                            title: "Confirmation",
                            size: "sm",
                            buttonSize: "sm",
                            okVariant: "success",
                            headerClass: "p-2 border-bottom-0",
                            footerClass: "p-2 border-top-0",
                            centered: true
                        });
                    } else {
                        self.$bvModal.hide("modal-auth");
                        self.$router.push({ name: "list" });
                    }
                }
            })
            .catch(function(error) {
                self.$notify({
                    group: "notify",
                    type: "error",
                    title: "Error",
                    text: error.message,
                    duration: 10000,
                    speed: 1000
                });
            });
    },
    sendVerifierCodeNew({ commit, state }) {
        axios
            .post(`${backendUrl}/api/suroo/citizens/verifiernewcode`, {
                userUuid: state.userUuid
            })
            .then(response => {
                if (response.data) {
                    commit("SHOW_NEW_CODE");
                }
            })
            .catch(function(error) {
                console.log(error);
            });
    },
    userAuth({ commit }, { userContact, self }) {
        return axios
            .post(`${backendUrl}/api/suroo/citizens/auth`, {
                userContact
            }, {
                timeout: 20000,
            })
            .then(response => {
                if (response.data) {
                    localStorage.setItem('userUuid', response.data.userUuid);
                    commit("SET_USER_UUID", response.data.userUuid, false);
                    localStorage.setItem('userContact', userContact);
                    commit('SET_USER_CONTACT', userContact);
                    self.$bvModal.show("modal-validate");
                }
            })
            .catch(function(error) {
                if (error.response) {
                    if (error.response.code === 400) {
                        self.$notify({
                            group: "notify",
                            type: "error",
                            title: "Error",
                            text: error.message,
                            duration: 10000,
                            speed: 1000
                        });
                    }
                } else {
                    console.log("Error", error.message);
                }
                throw new Error(error)
            });
    },
    userThemes({ commit, state }) {
        return new Promise((resolve, reject) => {
            axios
            .get(
                `${backendUrl}/api/suroo/citizens/themes/` +
                    state.userUuid +
                    "?userSession=" +
                    state.userSession
            )
            .then(response => {
                console.log(response);
                if (response.data) {
                    commit("SET_USER_THEMES_LIST", response.data);
                    resolve(response.data);
                }
            })
            .catch(function(error) {
                commit("RESET_USER");
                console.log("Error getting messages, your user was reset", error.message);
                reject(error)
            });
        });
    },
    userMessage({ commit, state }, { msg }) {
        console.warn('opening msg: ', msg);
        axios
            .get(
                `${backendUrl}/api/suroo/citizens/checkmsg/` +
                    state.userUuid +
                    "/" +
                    msg.msgId +
                    "?userSession=" +
                    state.userSession
            )
            .then(response => {
                commit('OPEN_MESSAGE', { msg, data: response.data });
                // const h = self.$createElement;
                // const titleVNode = h("div", {
                //     domProps: { innerHTML: response.data.theme }
                // });
                // const answer = JSON.parse(response.data.answer || {});
                // const bodyModal = [];
                // bodyModal.push(
                //     h("p", { class: ["text-left"] }, [response.data.message])
                // );
                // if (response.data.status) {
                //     bodyModal.push(
                //         h("p", { class: ["text-left"] }, [
                //             "Cтатус: " +
                //                 (response.data.stataus === 0
                //                     ? "доставлено"
                //                     : response.data.status === 1
                //                     ? "есть ответ"
                //                     : "отправка")
                //         ])
                //     );
                // }
                // if (response.data.fileHref) {
                //     bodyModal.push(
                //         h(
                //             "a",
                //             {
                //                 class: ["text-left"],
                //                 props: {
                //                     href: response.data.fileHref,
                //                     _target: "blank"
                //                 }
                //             },
                //             ["Файл"]
                //         )
                //     );
                // }
                // if (answer && answer.answerFilePath) {
                //     bodyModal.push(
                //         h(
                //             "a",
                //             {
                //                 class: ["text-left link-answer"],
                //                 props: {
                //                     href: answer.answerFilePath,
                //                     _target: "blank"
                //                 }
                //             },
                //             ["Ответ"]
                //         )
                //     );
                // }
                // const messageVNode = h("div", {}, bodyModal);
                // self.$bvModal.msgBoxOk([messageVNode], {
                //     title: [titleVNode],
                //     buttonSize: "md",
                //     centered: true,
                //     size: "md"
                // });
            });
    },
    closeMessage({ commit }) {
        commit('CLOSE_MESSAGE');
    },
    userAddSocial({ commit, state }, { data, self }) {
        axios
            .post(`${backendUrl}/api/social`, {
                userUuid: state.userUuid,
                userSession: state.userSession,
                snType: data.snType,
                userToken: data.userToken
            })
            .then(response => {
                if (response.code === 200) {
                }
            })
            .catch(function(error) {
                if (error.response) {
                    if (error.response.code === 400) {
                        commit("SHOW_CODE_ERROR", error.response.data.message);
                    }
                } else {
                    console.log("Error", error.message);
                }
            });
    },
    allAnswers({ commit, state }) {
        axios
            .get(
                `${backendUrl}/api/suroo/stats/answers/`
            )
            .then(response => {
                console.log('all answers', response);
            })
            .catch(function(error) {
                console.log(error);
            });
    },
};

export default actions;
