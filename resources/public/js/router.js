import Vue from "vue";
import Home from "./components/views/home/Home.vue";
import ListRequest from "./components/views/listRequest/ListRequest.vue";
import Privacy from "./components/views/Privacy.vue";
import Agreement from "./components/views/Agreement.vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
    { name: "home", path: "/", component: Home },
    { name: "list", path: "/list", component: ListRequest },
    { name: "privacy", path: "/privacy", component: Privacy },
    { name: "agreement", path: "/agreement", component: Agreement },
];

var router = new VueRouter({
    routes,
    mode: "history"
});

export default router;
