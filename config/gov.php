<?php

return [
  'domain' => env('MIX_API_DOMAIN', null),
  'username' => env('API_USERNAME', null),
  'password' => env('API_PASSWORD', null),
];
