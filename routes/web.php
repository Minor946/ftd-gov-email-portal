<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@home');
Route::get('/list', 'MainController@home');
Route::get('/privacy', 'MainController@home');
Route::get('/agreement', 'MainController@home');
Route::get('/deletion', 'MainController@deletion');
Route::post('sociallogin/{provider}', 'Auth\AuthController@SocialSignup');
Route::get('auth/{provider}/callback', 'OutController@index')->where('provider', '.*');

Route::get('/administrator', 'AdministratorController@home');
Route::get('/administrator/login', 'AdministratorController@home');
Route::get('/administrator/dashboard', 'AdministratorController@home');
Route::get('/administrator/users', 'AdministratorController@home');
Route::get('/administrator/goverments', 'AdministratorController@home');
